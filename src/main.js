/*
 * @Description: 
 * @Version: 2.0
 * @Autor: LMJ
 * @Date: 2021-09-27 13:40:21
 * @LastEditors: LMJ
 * @LastEditTime: 2021-09-27 14:21:50
 */
import Vue from 'vue'
import 'normalize.css/normalize.css'
import '@/styles/index.scss'
import App from './App'
import router from './router'
import Echarts from 'echarts'
import dataV from '@jiaminghi/data-view'
Vue.use(dataV)


Vue.config.productionTip = false

new Vue({
  el: '#app',
  router,
  render: h => h(App)
})
